﻿using BookTechSite.Models;
using BookTechSite.Services;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

// Testing the Repository against the BookTechTesting database 
// with minimal set of data
// using transactions to rollback after every test
namespace BookTechSite.IntegrationTests
{
    [TestFixture]
    public class AuthorRepositoryTests
    {
        [Test]
        public void should_get_a_count_of_authors()
        {
            var countOfAuthors = r.GetCountOfAuthors();
            Assert.AreEqual(4, countOfAuthors);
        }

        [Test]
        public void should_get_author_and_books_by_authorID()
        {
            AuthorBooksViewModel authorAndBooks = r.GetAuthorAndBooks(1);
            Assert.AreEqual("John", authorAndBooks.FirstName);
            Assert.AreEqual("Tolkien", authorAndBooks.LastName);
            Assert.AreEqual("The Hobbit", authorAndBooks.Books.ElementAt(0).Title);
            Assert.AreEqual("The Lord of The Rings", authorAndBooks.Books.ElementAt(1).Title);
        }

        [Test]
        public void should_get_a_list_of_all_authors_and_their_books()
        {
            IEnumerable<AuthorBooksViewModel> result = r.GetAuthorsAndBookTitles();
            var authorAndBooks = result.First();
            Assert.AreEqual("Agatha", authorAndBooks.FirstName);
            Assert.AreEqual("Christie", authorAndBooks.LastName);
            Assert.AreEqual("And Then There Were None", authorAndBooks.Books.ElementAt(0).Title);
        }

        [Test]
        public void should_return_authorID_on_create_of_author()
        {
            var authorID = CreateTestAuthorBobSmith();

            Assert.IsNotNull(authorID);
            Assert.AreNotEqual(authorID, 0);
        }

        [Test]
        public void should_return_author_with_populated_authorID_when_created()
        {
            var author = new Author
            {
                FirstName = "Bob",
                LastName = "Smith",
                EmailAddress = "Bob@Smith.com"
            };
            r.Create(author);

            Assert.AreNotEqual(0, author.AuthorID);
        }

        [Test]
        public void should_get_author_given_id()
        {
            var author = r.GetAuthorByID(1);

            Assert.AreEqual("John", author.FirstName);
        }

        [Test]
        public void should_return_rowling_when_search_for_rowl()
        {
            var authors = r.SearchByLastName("rowl");
            Assert.AreEqual(1, authors.Count());
        }

        [Test]
        public void should_return_0_when_search_for_author_lastname_asdf()
        {
            var authors = r.SearchByLastName("asdf");
            Assert.AreEqual(0, authors.Count());
        }

        [Test]
        public void should_return_null_when_author_not_found()
        {
            Author author = r.GetAuthorByID(123456);
            Assert.IsNull(author);
        }

        [Test]
        public void should_update_firstname_when_update_is_called()
        {
            var author = CreateTestAuthorBobSmith();

            var author2 = r.GetAuthorByID(author.AuthorID);

            author2.FirstName = "Bob2";
            r.UpdateAuthor(author2);

            var authorFromDB = r.GetAuthorByID(author.AuthorID);
            Assert.AreEqual("Bob2", authorFromDB.FirstName);
        }

        Author CreateTestAuthorBobSmith()
        {
            var author = new Author
            {
                FirstName = "Bob",
                LastName = "Smith",
                EmailAddress = "Bob@Smith.com"
            };
            r.Create(author);
            return author;
        }

        [Test]
        public void should_hard_delete_author_given_an_id()
        {
            var author = CreateTestAuthorBobSmith();

            r.Delete(author.AuthorID);

            var authorFromDB = r.GetAuthorByID(author.AuthorID);
            Assert.IsNull(authorFromDB);
        }

        TransactionScope scope;
        AuthorRepository r;
        const bool RollBackTransaction = true;

        // Before each test
        [SetUp]
        public void SetUp()
        {
            r = new AuthorRepository();
            if (RollBackTransaction) scope = new TransactionScope();
        }

        // After each test
        [TearDown]
        public void TearDown()
        {
            if (RollBackTransaction) scope.Dispose();
        }
    }
}
