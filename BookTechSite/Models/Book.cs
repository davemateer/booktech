﻿using System.ComponentModel.DataAnnotations;

namespace BookTechSite.Models
{
    public class Book 
    {
        public int BookID { get; set; }

        // Are we storing nvarchar or varchar???
        // hmm titles may be internationalised so stick with nvarchar in the db
        [Required]
        [StringLength(1024)]
        public string Title { get; set; }

        [Required]
        public int AuthorID { get; set; }

        // 900m is the max, 0 is the default as we don't allow nulls.
        // 0 constrained as the default in the db too
        //[Range(0,900000000, ErrorMessage = "Must be between 1 and 900million")]
        //[DefaultValue(0)]
        //public int NumberOfSales { get; set; }
        
        public bool IsFirstBookSale()
        {
            return true;
        }
    }
}