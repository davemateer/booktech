﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookTechSite.Models
{
    public enum AuthorSaleStatus
    {
        AuthorNotPaid = 1,
        AuthorPaid = 2
    }

    public class Sale
    {
        public int SaleID { get; set; }
        [Required]
        public int BookID { get; set; }
        [Required]
        public double Price { get; set; }
        [Required]
        public DateTime SaleDate { get; set; }

        // Author Not Paid, Author Paid
        [Required]
        public int AuthorSaleStatus { get; set; }

    }
}