using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookTechSite.Models
{
    public class AuthorBooksViewModel
    {
        [Key]
        public int AuthorID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<Book> Books { get; set; }
    }
}