﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BookTechSite.Models
{
    // Author is currently what is used by /Authors/Create  and /Authors/Edit/1
    public class Author
    {
        public int AuthorID { get; set; }
        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(255)]
        public string LastName { get; set; }

        [Required]
        [StringLength(255)]
        public string EmailAddress { get; set; }


        public IEnumerable<Book> Books { get; set; }
       
    }
}