using System.ComponentModel.DataAnnotations;

namespace BookTechSite.Models
{
    public class BooksViewModel
    {
        [Key]
        public int BookID { get; set; }
        public string Title { get; set; }
        public int AuthorID { get; set; }
        public string AuthorFirstName { get; set; }
        public string AuthorLastName { get; set; }
    }
}