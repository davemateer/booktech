﻿using BookTechSite.Services;
using System.Web.Mvc;
using BookTechSite.Models;

namespace BookTechSite.Controllers
{
    public class AuthorsController : Controller
    {
        IAuthorRepository repo;

        public AuthorsController(IAuthorRepository repo)
        {
            this.repo = repo;
        }

        public ViewResult Index()
        {
            ViewBag.CountOfAuthors = repo.GetCountOfAuthors();
            return View(repo.GetAuthorsAndBookTitles());
        }

        public ActionResult LoadDataSqlBulkCopyAuthorsBooks()
        {
            repo.LoadDataSqlBulkCopyAuthorsBooks();
            return RedirectToAction("index");
        }

        public ViewResult Details(int id)
        {
            AuthorBooksViewModel authorBooksVM = repo.GetAuthorAndBooks(id);
            //return authorBooksVM == null ? EntityNotFound() : View(authorBooksVM);
            return View(authorBooksVM);
        }

        [HttpPost]
        public ActionResult Details(AuthorBooksViewModel vm)
        {
            // when Authorise payment button is pressed

            // if the tick box has been pressed (almost view logic, so leave it in the controller)
            //var service = new BPAService();
            //service.AuthorisePaymentToAuthorOnAllTheirBooksWhichAreOutstanding(vm.AuthorID);

            return RedirectToAction("index");
        }

        // Using a GET so URL is copyable
        [HttpGet]
        public ActionResult Search(string lastName = "")
        {
            var authors = repo.SearchByLastName(lastName);
            return View(authors);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Author author)
        {
            if (ModelState.IsValid)
            {
                repo.Create(author);
                return RedirectToAction("index");
            }
            return View(author);
        }

        public ActionResult Edit(int id)
        {
            var author = repo.GetAuthorByID(id);
            //if (author == null) return EntityNotFound();
            return View(author);
        }

        [HttpPost]
        public ActionResult Edit(Author author)
        {
            if (ModelState.IsValid)
            {
                repo.UpdateAuthor(author);
                return RedirectToAction("index");
            }

            return View(author);
        }

        public ActionResult Delete(int authorID)
        {
            repo.Delete(authorID);
            return RedirectToAction("index");
        }
    }
}