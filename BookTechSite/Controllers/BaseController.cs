﻿using System.Web.Mvc;

namespace BookTechSite.Controllers
{
    // BaseController for Controllers
    // http://stackoverflow.com/a/499907/26086
    public class BaseController : Controller
    {
        protected ViewResult EntityNotFound()
        {
            Response.StatusCode = 404;
            return View("EntityNotFoundError");
        }
    }
}