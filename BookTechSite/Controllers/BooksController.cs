﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.UI;
using BookTechSite.Models;
using BookTechSite.Services;

namespace BookTechSite.Controllers
{
    public class BooksController : Controller
    {
        BookRepository repo = new BookRepository();

        public ActionResult Index()
        {
            ViewBag.CountOfBooks = repo.GetCountOfBooks();
            return View(repo.GetBooks());
        }

        // GET: Books/Details/5
        public ActionResult Details(int id)
        {
            var book = repo.GetBookByBookID(id);
            return View(book);
        }

        // GET: Books/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        // GET: Books/Create?authorID=1
        public ActionResult Create(int authorID = 0)
        {
            return View();
        }

        // POST: Books/Create
        [HttpPost]
        public ActionResult Create(Book book)
        {
            if (ModelState.IsValid)
            {
                repo.Create(book);
                return RedirectToAction("index");
            }
            return View(book);
        }

        // GET: Books/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Books/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Books/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
