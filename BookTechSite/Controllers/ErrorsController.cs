﻿using System;
using System.Reflection;
using System.Web.Mvc;

namespace BookTechSite.Controllers
{
    public class ErrorsController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            //if (exception == null) return;

            var ex = (Exception)RouteData.Values["ex"];

            //log.Error(Request.RawUrl);
            //log.Error(exception.StackTrace);
            ViewBag.ExceptionMessage = ex.Message;
            ViewBag.InnerExceptionMessage = ex.InnerException != null ? ex.InnerException.Message : "";
            ViewBag.Stacktrace = ex.StackTrace;
            ViewBag.RawURL = RouteData.Values["rawurl"];

            return View("Index");
        }

        ///// <summary>
        ///// All other errors
        ///// </summary>
        ///// <param name="actionName"></param>
        //protected override void HandleUnknownAction(string actionName)
        //{
        //    // in case detailed exception is required.
        //    var ex = (Exception)RouteData.Values["ex"];
        //    return View();
        //}
    }
}