﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using BookTechSite.Models;
using Dapper;

namespace BookTechSite.Services
{
    public class BookRepository
    {
        public IEnumerable<Book> GetBooks()
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<Book>("GetBooks", commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<Book> GetBooksByAuthorID(int authorID)
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<Book>("GetBooksByAuthorID", new { authorID }, commandType: CommandType.StoredProcedure);
            }
        }

        public Book GetBookByBookID(int bookID)
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<Book>("GetBookByBookID", new { bookID }, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }


        public int Create(Book book)
        {
            using (var db = Util.GetOpenConnection())
            {
                var p = new DynamicParameters();
                p.Add("@BookID", book.BookID, direction: ParameterDirection.Output);
                p.Add("@Title", book.Title);
                p.Add("@AuthorID", book.AuthorID);
                //p.Add("@NumberOfSales", book.NumberOfSales);

                db.Execute("CreateBook", p, commandType: CommandType.StoredProcedure);
                return p.Get<int>("@BookID");
            }
        }

        public int GetCountOfBooks()
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<int>("GetCountOfBooks", commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }
    }
}