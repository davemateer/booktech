using StackExchange.Profiling;
using StackExchange.Profiling.Data;
using StackExchange.Profiling.SqlFormatters;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace BookTechSite.Services
{
    public static class Util
    {
        // Appveyor is configured to set AppveyorSQL2014ConnectionString in appveyor.yml
        // Otherwise get the value from web.config (BookTech) in MVC project
        // or app.config (BookTechTesting) in IntegrationTests project
        static readonly string ConnectionString =
            Environment.GetEnvironmentVariable("AppveyorSQL2014ConnectionString") ??
            ConfigurationManager.ConnectionStrings["BookTechConnectionString"].ConnectionString;

        public static IDbConnection GetOpenConnection()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            MiniProfiler.Settings.SqlFormatter = new SqlServerFormatter();
            return new ProfiledDbConnection(connection, MiniProfiler.Current);
        }

        public static string CapitaliseFirstLetter(this string s)
        {
            if (String.IsNullOrEmpty(s)) return s;
            if (s.Length == 1) return s.ToUpper();
            return s.Remove(1).ToUpper() + s.Substring(1);
        }

        // To stop similar random numbers use the same Random instance
        private static Random rnd;
        public static Random GetRandom()
        {
            if (rnd != null) return rnd;
            rnd = new Random();
            return rnd;
        }


    }
}