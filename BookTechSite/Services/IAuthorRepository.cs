﻿using System.Collections.Generic;
using System.Data;
using BookTechSite.Models;

namespace BookTechSite.Services
{
    public interface IAuthorRepository
    {
        IEnumerable<AuthorBooksViewModel> GetAuthorsAndBookTitles();
        int GetCountOfAuthors();
        AuthorBooksViewModel GetAuthorAndBooks(int authorID);
        Author GetAuthorByID(int authorID);
        IEnumerable<Author> SearchByLastName(string lastName);
        void Create(Author author);
        void UpdateAuthor(Author author);
        void Delete(int authorID);
        void LoadDataSqlBulkCopyAuthorsBooks();
        DataTable BuildDataTableAuthors(int from, int count, List<string> firstnames, List<string> surnames, List<string> words);
        Author MakeRandomAuthor(List<string> firstnames, List<string> surnames);
        List<Book> MakeRandomNumberOfBooks(List<string> words);
        Book GetBookTitle(List<string> words);
    }
}