﻿using BookTechSite.Models;
using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace BookTechSite.Services
{
    public class AuthorRepository : IAuthorRepository
    {
        public IEnumerable<AuthorBooksViewModel> GetAuthorsAndBookTitles()
        {
            using (var db = Util.GetOpenConnection())
            {
                var authorsAndBookTitles = db.QueryMultiple("GetAuthorsAndBookTitles", commandType: CommandType.StoredProcedure);
                var authors = authorsAndBookTitles.Read<Author>().ToList();
                var books = authorsAndBookTitles.Read<Book>().ToList();

                var list = new List<AuthorBooksViewModel>();
                foreach (var author in authors)
                {
                    list.Add(new AuthorBooksViewModel
                    {
                        AuthorID = author.AuthorID,
                        FirstName = author.FirstName,
                        LastName = author.LastName,
                        Books = books.Where(x => x.AuthorID == author.AuthorID)
                    });
                }
                return list;
            }
        }

        public int GetCountOfAuthors()
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<int>("GetCountOfAuthors", commandType: CommandType.StoredProcedure).FirstOrDefault();
            }
        }

        public AuthorBooksViewModel GetAuthorAndBooks(int authorID)
        {
            using (var db = Util.GetOpenConnection())
            {
                var authorAndBooks = db.QueryMultiple("GetAuthorAndBooks", new { authorID }, commandType: CommandType.StoredProcedure);
                var author = authorAndBooks.Read<Author>().Single();
                var books = authorAndBooks.Read<Book>().ToList();

                return new AuthorBooksViewModel
                {
                    AuthorID = author.AuthorID,
                    FirstName = author.FirstName,
                    LastName = author.LastName,
                    Books = books
                };
            }
        }

        public Author GetAuthorByID(int authorID)
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<Author>("GetAuthorByID", new { authorID }, commandType: CommandType.StoredProcedure).SingleOrDefault();
            }
        }

        public IEnumerable<Author> SearchByLastName(string lastName)
        {
            using (var db = Util.GetOpenConnection())
            {
                return db.Query<Author>("SearchAuthorByLastName", new { lastName }, commandType: CommandType.StoredProcedure);
            }
        }

        // Populating author with its ID as a side effect of passing author by ref
        public void Create(Author author)
        {
            using (var db = Util.GetOpenConnection())
            {
                var p = new DynamicParameters();
                p.Add("@authorID", author.AuthorID, direction: ParameterDirection.Output);
                p.Add("@firstName", author.FirstName);
                p.Add("@lastName", author.LastName);
                p.Add("@EmailAddress", author.EmailAddress);

                db.Execute("CreateAuthor", p, commandType: CommandType.StoredProcedure);
                var authorID = p.Get<int>("@authorID");
                author.AuthorID = authorID;
            }
        }

        public void UpdateAuthor(Author author)
        {
            using (var db = Util.GetOpenConnection())
            {
                var p = new DynamicParameters();
                p.Add("@authorID", author.AuthorID);
                p.Add("@firstName", author.FirstName);
                p.Add("@lastName", author.LastName);
                db.Execute("UpdateAuthor", p, commandType: CommandType.StoredProcedure);
            }
        }

        public void Delete(int authorID)
        {
            using (var db = Util.GetOpenConnection())
            {
                db.Execute("DeleteAuthor", new { authorID }, commandType: CommandType.StoredProcedure);
            }
        }

        public void LoadDataSqlBulkCopyAuthorsBooks()
        {
            // 20 took 10s on Azure (1600 total)
            // 60 took 30s on Azure
            // 120 took 42s on Azure

            // batch count = 160
            // 20 took 20s on Azure (3200 total)

            // numberInBatch 1000, number of batches=5  (5000 total).  took 5s
            // numberInBatch 2000, (10000 total).  took 8.8s
            // numberInBatch 5000, (25,000 total).  took 21s

            // numberInBatch 10,000 (10,000 total) took 7s
            // numberInBatch 25,000 (25,000 total) took 18s

            // 10 * 10,000.. took 238s in Azure
            // 10* 25,000 - timeouts
            // 25 * 10,000..

            // load time of 250,000 went from 6s to 2s changing DTU's from 5 to 10.
            // load time of 215ms from 10 to 100DTU's (same as local on SSD's)

            var numberInBatch = 10000;
            var numberOfBatches = 25;

            DropConstraintsAndTruncateTables();

            var firstnames = LoadFirstNames();
            var surnames = LoadSurnames();
            var words = LoadWords();

            var dataTablesAuthor = new List<DataTable>();
            for (var i = 0; i < numberOfBatches; i++)
                dataTablesAuthor.Add(BuildDataTableAuthors(i * numberInBatch, numberInBatch, firstnames, surnames, words));

            // Add Authors
            foreach (var dt in dataTablesAuthor)
            {
                using (var s = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["BookTechConnectionString"].ConnectionString))
                {
                    s.DestinationTableName = "Authors";
                    s.BatchSize = 0;
                    s.WriteToServer(dt);
                    s.Close();
                }
            }

            var dataTablesBooks = new List<DataTable>();
            for (var i = 0; i < numberOfBatches; i++)
            {
                dataTablesBooks.Add(BuildDataTableBooks(i*numberInBatch, numberInBatch, words));
            }
            // Add Books - we know the range of AuthorID's, so can assign books appropriately
            foreach (var dt in dataTablesBooks)
            {
                using (var s = new SqlBulkCopy(ConfigurationManager.ConnectionStrings["BookTechConnectionString"].ConnectionString))
                {
                    s.DestinationTableName = "Books";
                    s.BatchSize = 0;
                    s.WriteToServer(dt);
                    s.Close();
                }
            }

            using (var c = Util.GetOpenConnection())
            {
                c.Execute("ALTER TABLE[dbo].[Books] WITH CHECK ADD CONSTRAINT[FK_Books_ToAuthor] FOREIGN KEY([AuthorID]) REFERENCES[dbo].[Authors]([AuthorID])");
            }
        }

        private static List<string> LoadWords()
        {
            var p3 = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "words.txt");
            var words = File.ReadAllLines(p3).ToList();
            return words;
        }

        private static List<string> LoadSurnames()
        {
            var p2 = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "surnames.txt");
            var surnames = File.ReadAllLines(p2).ToList();
            return surnames;
        }

        private static List<string> LoadFirstNames()
        {
            var p1 = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "firstnames.txt");
            var firstnames = File.ReadAllLines(p1).ToList();
            return firstnames;
        }

        private static void DropConstraintsAndTruncateTables()
        {
            using (var c = Util.GetOpenConnection())
            {
                try
                {
                    c.Execute("ALTER TABLE [dbo].[Books] DROP CONSTRAINT [FK_Books_ToAuthor]");
                }
                catch
                {
                }

                c.Execute("TRUNCATE TABLE Books; TRUNCATE TABLE Authors");
            }
        }

        private DataTable BuildDataTableBooks(int from, int count, List<string> words)
        {
            var dt = new DataTable();
            dt.Columns.Add("BookID", typeof(int));
            dt.Columns.Add("Title", typeof(string));
            dt.Columns.Add("AuthorID", typeof(int));
            int counter = 1;
            for (int i = from; i < from + count; i++)
            {
                List<Book> books = MakeRandomNumberOfBooks(words);
                foreach (var book in books)
                {
                    DataRow row = dt.NewRow();
                    row.ItemArray = new object[] { counter, book.Title, i + 1 };
                    dt.Rows.Add(row);
                    counter++;
                }
            }
            return dt;
        }

        public DataTable BuildDataTableAuthors(int from, int count, List<string> firstnames, List<string> surnames, List<string> words)
        {
            var dt = new DataTable();
            dt.Columns.Add("AuthorID", typeof(int));
            dt.Columns.Add("FirstName", typeof(string));
            dt.Columns.Add("LastName", typeof(string));
            dt.Columns.Add("EmailAddress", typeof(string));

            for (int i = from; i < from + count; i++)
            {
                Author author = MakeRandomAuthor(firstnames, surnames);
                DataRow row = dt.NewRow();
                // this looks like AuthorID initially will be 0, however it is 1 (identity).. so its just a key for the dt?
                row.ItemArray = new object[] { i, author.FirstName, author.LastName, author.EmailAddress };
                dt.Rows.Add(row);
            }
            return dt;
        }

        public Author MakeRandomAuthor(List<string> firstnames, List<string> surnames)
        {
            var r = Util.GetRandom();
            string firstname = firstnames[r.Next(firstnames.Count)].CapitaliseFirstLetter();
            string surname = surnames[r.Next(surnames.Count)].CapitaliseFirstLetter();
            string email = firstname + "@" + surname + ".com";
            return new Author { FirstName = firstname, LastName = surname, EmailAddress = email };
        }

        public List<Book> MakeRandomNumberOfBooks(List<string> words)
        {
            var books = new List<Book>();
            for (int j = 0; j < Util.GetRandom().Next(1, 5); j++)
            {
                Book book = GetBookTitle(words);
                books.Add(book);
            }
            return books;
        }

        public Book GetBookTitle(List<string> words)
        {
            var r = Util.GetRandom();
            int firstWordIndex = r.Next(words.Count);
            string firstWord = words[firstWordIndex].CapitaliseFirstLetter();

            // unless want to run out of words!
            //words.RemoveAt(firstWordIndex);

            int secondWordIndex = r.Next(words.Count);
            string secondWord = words[secondWordIndex].CapitaliseFirstLetter();

            var book = new Book { Title = firstWord + " of the " + secondWord };
            return book; //xx
        }

        // ADO.NET DbNull datareader test
        //public void GetThings()
        //{
        //    var connectionString = ConfigurationManager.ConnectionStrings["BookTechConnectionString"].ConnectionString;
        //    using (var connection = new SqlConnection(connectionString))
        //    {
        //        using (var cmd = new SqlCommand("SELECT * FROM Things", connection))
        //        {
        //            connection.Open();
        //            using (var reader = cmd.ExecuteReader())
        //            {
        //                while (reader.Read())
        //                {
        //                    var thingID = reader["ThingID"].ToString();
        //                    var name= reader["Name"].ToString();

        //                    // Catches the DBNull
        //                    if (reader["OtherInfo"] == DBNull.Value)
        //                    {
        //                        var x = 1;
        //                    }
        //                    // .ToString() always gives a String.Empty if source value is DbNull
        //                    var otherInfo = reader["OtherInfo"].ToString();

        //                    int thingIDInt;
        //                    int.TryParse(thingID, out thingIDInt);

        //                }
        //            }
        //        }
        //    }
        //}

        // ADO.NET using Dapper with SQL
        // Dapper's primary feature is mapping from .NET classes to database tables
        //public IList<Author> GetAuthorsDapper()
        //{
        //    List<Author> authors;
        //    using (var db = Util.GetOpenConnection())
        //    {
        //        authors = db.Query<Author>("SELECT * FROM Author").ToList();
        //    }
        //    return authors;
        //}

        // ADO.NET using SP
        //public IList<Author> GetAuthors_ADO_SP()
        //{
        //    var list = new List<Author>();
        //    var connectionString = ConfigurationManager.ConnectionStrings["BookTechConnectionString"].ConnectionString;
        //    using (var connection = new SqlConnection(connectionString))
        //    {
        //        using (var cmd = new SqlCommand("GetAuthors", connection))
        //        {
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            connection.Open();
        //            using (var reader = cmd.ExecuteReader())
        //            {
        //                while (reader.Read())
        //                {
        //                    var authorID = reader["AuthorID"].ToString();
        //                    var firstName = reader["FirstName"].ToString();
        //                    var lastName = reader["LastName"].ToString();

        //                    int authorIDInt;
        //                    int.TryParse(authorID, out authorIDInt);

        //                    list.Add(new Author { AuthorID = authorIDInt, FirstName = firstName, LastName = lastName });
        //                }
        //            }
        //        }
        //    }
        //    return list;
        //}

        //public void DeleteAllAuthors_RawSQL()
        //{
        //    var connectionString = ConfigurationManager.ConnectionStrings["BookTechConnectionString"].ConnectionString;
        //    using (var connection = new SqlConnection(connectionString))
        //    {
        //        using (var cmd = new SqlCommand("DELETE FROM Author", connection))
        //        {
        //            connection.Open();
        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //}

        // ADO.NET raw SQL
        //public IList<Author> GetAuthors_ADO_SQL()
        //{
        //    var list = new List<Author>();
        //    var connectionString = ConfigurationManager.ConnectionStrings["BookTechConnectionString"].ConnectionString;
        //    using (var connection = new SqlConnection(connectionString))
        //    {
        //        using (var cmd = new SqlCommand("SELECT * FROM Author", connection))
        //        {
        //            connection.Open();
        //            using (var reader = cmd.ExecuteReader())
        //            {
        //                while (reader.Read())
        //                {
        //                    var authorID = reader["AuthorID"].ToString();
        //                    var firstName = reader["FirstName"].ToString();
        //                    var lastName = reader["LastName"].ToString();

        //                    int authorIDInt;
        //                    int.TryParse(authorID, out authorIDInt);

        //                    list.Add(new Author { AuthorID = authorIDInt, FirstName = firstName, LastName = lastName });
        //                }
        //            }
        //        }
        //    }
        //    return list;
        //}



    }

}