﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using BookTechSite.Controllers;
using log4net;
using StackExchange.Profiling;

namespace BookTechSite
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Patch in our own Factory/Composition Root into MVC pipeline
            ControllerBuilder.Current.SetControllerFactory(new BookTechControllerFactory());

            // Miniprofiler helpers
            // Setup profiler for Controllers via a Global ActionFilter
            //GlobalFilters.Filters.Add(new ProfilingActionFilter());

            //// Initialize automatic view profiling
            //var copy = ViewEngines.Engines.ToList();
            //ViewEngines.Engines.Clear();
            //foreach (var item in copy)
            //{
            //    ViewEngines.Engines.Add(new ProfilingViewEngine(item));
            //}
        }

        protected void Application_BeginRequest()
        {
            //if (Request.IsLocal)
                MiniProfiler.Start();
        }

        protected void Application_EndRequest()
        {
            //if (Request.IsLocal)
                MiniProfiler.Stop();
        }

        // For an internal business app want to log the error
        // and show the user some debug information
        // not worried about response codes eg for search engines

        // this will catch any Exceptions

        // any error on Entities eg /Author/123123, when the ID isn't there, returns to base controller
        // which logs, and redirects to EntitiesNotFoundError
        protected void Application_Error(object sender, EventArgs e)
        {
            var currentController = " ";
            var currentAction = " ";
            var currentRouteData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(Context));

            if (currentRouteData != null)
            {
                if (currentRouteData.Values["controller"] != null && !String.IsNullOrEmpty(currentRouteData.Values["controller"].ToString()))
                    currentController = currentRouteData.Values["controller"].ToString();

                if (currentRouteData.Values["action"] != null && !String.IsNullOrEmpty(currentRouteData.Values["action"].ToString()))
                    currentAction = currentRouteData.Values["action"].ToString();
            }

            Exception ex = Server.GetLastError();
            if (ex == null) return;

            ILog log = LogManager.GetLogger(typeof(MvcApplication));
            log.Error(ex.Message);
            log.Error(Request.RawUrl);
            log.Error(ex.StackTrace);
            Server.ClearError();

            var routeData = new RouteData();
            routeData.Values["controller"] = "Errors";
            routeData.Values["action"] = "Index";
            routeData.Values["ex"] = ex;
            routeData.Values["rawurl"] = Request.RawUrl;

            var errorController = new ErrorsController();

            errorController.ViewData.Model = new HandleErrorInfo(ex, currentController, currentAction);
            ((IController)errorController).Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}
