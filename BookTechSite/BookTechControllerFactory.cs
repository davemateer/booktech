﻿using BookTechSite.Controllers;
using BookTechSite.Services;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace BookTechSite
{
    // http://stackoverflow.com/a/32033303/26086
    public class BookTechControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == typeof (AuthorsController))
            {
                var repo = new AuthorRepository();
                return new AuthorsController(repo);
            }

            // AccountController will still go the normal tightly coupled way
            return base.GetControllerInstance(requestContext, controllerType);
        }
    }
}