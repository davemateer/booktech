﻿using System.Web.Mvc;
using System.Web.Routing;

namespace BookTechSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // /Authors/1  is the same as /Authors/Details/1
            routes.MapRoute(
               name: "Details Shortcut",
               url: "Authors/{id}",
               defaults: new { controller = "Authors", action = "Details" },
               constraints: new { id = @"\d+" } // \d+ matches one or more integers
           );

            // /Books/1  is the same as /Books/Details/1
            routes.MapRoute(
               name: "Books Shortcut",
               url: "Books/{id}",
               defaults: new { controller = "Books", action = "Details" },
               constraints: new { id = @"\d+" } // \d+ matches one or more integers
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Authors", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
