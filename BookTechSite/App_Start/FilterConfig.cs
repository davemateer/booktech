﻿using System.Web.Mvc;

namespace BookTechSite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            // Handle all errors via Application_Error in global.asax
            filters.Add(new HandleErrorAttribute());
        }
    }
}
