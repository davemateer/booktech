﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--USE BookTech
--GO

-- Only insert sample data if database is blank
-- which is the case on the Appveyor integration tests database
-- but not on production
-- https://en.wikipedia.org/wiki/List_of_best-selling_books#More_than_100_million_copies

IF NOT EXISTS (SELECT TOP 1 * FROM [Authors])
BEGIN
	SET IDENTITY_INSERT [dbo].[Authors] ON 
	INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [EmailAddress]) VALUES (1, N'John', N'Tolkien', 'john@tolkien.com')
	INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [EmailAddress]) VALUES (2, N'Antoine', N'de Saint-Exupery', 'a@se.com')
	INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [EmailAddress]) VALUES (3, N'Jo', N'Rowling', 'j@rowling.com')
	INSERT [dbo].[Authors] ([AuthorID], [FirstName], [LastName], [EmailAddress]) VALUES (4, N'Agatha', N'Christie','ag@christie.com')
	SET IDENTITY_INSERT [dbo].[Authors] OFF
END

IF NOT EXISTS (SELECT TOP 1 * FROM Books)
BEGIN
	SET IDENTITY_INSERT [dbo].Books ON 
	INSERT [dbo].Books (BookID, Title, AuthorID) VALUES (1, N'The Hobbit', 1)
	INSERT [dbo].Books (BookID, Title, AuthorID) VALUES (2, N'The Lord of The Rings', 1)
	INSERT [dbo].Books (BookID, Title, AuthorID) VALUES (3, N'Le Petit Prince', 2)
	INSERT [dbo].Books (BookID, Title, AuthorID) VALUES (4, N'Harry Potter and the Philosophers Stone', 3)
	INSERT [dbo].Books (BookID, Title, AuthorID) VALUES (5, N'Harry Potter and the Half Blood Prince', 3)
	INSERT [dbo].Books (BookID, Title, AuthorID) VALUES (6, N'Harry Potter and the Chamber of Secrets', 3)
	INSERT [dbo].Books (BookID, Title, AuthorID) VALUES (7, N'And Then There Were None', 4)
	SET IDENTITY_INSERT [dbo].[Books] OFF
END

--IF NOT EXISTS (SELECT * FROM Sales)
--BEGIN
--	SET IDENTITY_INSERT [dbo].Sales ON 
--	INSERT [dbo].Sales (SaleID, BookID, Price, SaleDate) VALUES (1, 2, 7.99, '2015-02-20 09:00:00')
--	INSERT [dbo].Sales (SaleID, BookID, Price, SaleDate) VALUES (2, 2, 7.99, '2015-02-21 10:00:00')
--	SET IDENTITY_INSERT [dbo].[Sales] OFF
--END
