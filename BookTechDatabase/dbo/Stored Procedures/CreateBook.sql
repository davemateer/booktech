﻿CREATE PROCEDURE [dbo].[CreateBook]
	@Title nvarchar(1024),
	@AuthorID int,
	@BookID int output
AS
BEGIN	
	INSERT INTO Books(Title, AuthorID)
	VALUES (@Title, @AuthorID)

	SET @BookID = SCOPE_IDENTITY()
END
