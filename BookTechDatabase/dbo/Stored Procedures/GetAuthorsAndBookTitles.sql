﻿
CREATE PROCEDURE GetAuthorsAndBookTitles
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 30 * 
	INTO #Authors
	FROM Authors
	ORDER BY LastName

	SELECT * FROM #Authors

	SELECT * FROM Books
	WHERE AuthorID IN (SELECT AuthorID FROM #Authors)
	
END
