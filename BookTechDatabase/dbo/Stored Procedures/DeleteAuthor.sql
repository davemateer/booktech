﻿CREATE PROCEDURE [dbo].[DeleteAuthor]
    @AuthorID int
AS
BEGIN
	DELETE FROM [Authors]
	WHERE AuthorID = @AuthorID
END
