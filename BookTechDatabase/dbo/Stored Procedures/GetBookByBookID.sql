﻿CREATE PROCEDURE [dbo].[GetBookByBookID]
	@BookID int
AS
	SELECT * FROM Books
	WHERE BookID = @BookID

RETURN 0
