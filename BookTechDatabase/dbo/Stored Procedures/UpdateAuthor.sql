﻿CREATE PROCEDURE [dbo].[UpdateAuthor]
    @AuthorID int,
	@FirstName nvarchar(255),
	@LastName nvarchar(255)
AS
BEGIN
	UPDATE [Authors]
	SET 
	FirstName = @FirstName
	,LastName = @LastName
	WHERE AuthorID = @AuthorID
END
