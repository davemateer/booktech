﻿CREATE PROCEDURE [dbo].[CreateAuthor]
	@FirstName nvarchar(255),
	@LastName nvarchar(255),
	@EmailAddress nvarchar(255),
	@AuthorID int output
AS
BEGIN	
	INSERT INTO [Authors](FirstName, LastName,EmailAddress)
	VALUES (@FirstName, @LastName, @EmailAddress)

	SET @AuthorID = SCOPE_IDENTITY()
END
