﻿CREATE TABLE [dbo].[Books]
(
	[BookID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] NVARCHAR(1024) NOT NULL, 
    [AuthorID] INT NOT NULL, 
    CONSTRAINT [FK_Books_ToAuthor] FOREIGN KEY (AuthorID) REFERENCES Authors([AuthorID]) 
)
