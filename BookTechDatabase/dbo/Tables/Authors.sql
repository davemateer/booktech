﻿CREATE TABLE [dbo].[Authors] (
    [AuthorID]  INT           IDENTITY (1, 1) NOT NULL,
    [FirstName] NVARCHAR (255) NOT NULL,
    [LastName]  NVARCHAR (255) NOT NULL,
    [EmailAddress] NVARCHAR(255) NOT NULL, 
    CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED ([AuthorID] ASC)
);

