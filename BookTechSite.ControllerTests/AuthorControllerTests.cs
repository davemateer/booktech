﻿using BookTechSite.Controllers;
using BookTechSite.Models;
using BookTechSite.Services;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace BookTechSite.ControllerTests
{
    [TestFixture]
    public class AuthorControllerTests
    {
        Mock<IAuthorRepository> repo;
        AuthorsController controller;

        [SetUp]
        public void Setup()
        {
            var inMemoryAuthorsList = new List<AuthorBooksViewModel>
            {
                new AuthorBooksViewModel
                {
                    AuthorID = 1,
                    FirstName = "Dave",
                    LastName = "Mateer"
                }
            };

            repo = new Mock<IAuthorRepository>();

            repo.Setup(x => x.GetAuthorsAndBookTitles())
                .Returns(inMemoryAuthorsList);

            repo.Setup(x => x.GetAuthorAndBooks(It.IsAny<int>()))
                .Returns(inMemoryAuthorsList[0]);

            controller = new AuthorsController(repo.Object);
        }

        [Test]
        public void index_should_render_default_view()
        {
            var result = controller.Index();

            // IEnumerable<AuthorBooksViewModel>
            Assert.IsNotNull(result.Model);
        }

        [Test]
        public void index_should_return_Authors_and_Books()
        {
            object result = controller.Index().ViewData.Model;

            repo.Verify(x => x.GetAuthorsAndBookTitles(), Times.Once);
            Assert.IsInstanceOf(typeof(IEnumerable<AuthorBooksViewModel>), result);

            var model = (IEnumerable<AuthorBooksViewModel>)result;
            Assert.AreEqual(1, model.Count());
        }

        [Test]
        public void details_should_return_the_correct_Author_and_Books()
        {
            int authorID = 1;
            var result = (AuthorBooksViewModel)controller.Details(authorID).ViewData.Model;

            repo.Verify(x => x.GetAuthorAndBooks(authorID), Times.Once);
            Assert.IsInstanceOf(typeof(AuthorBooksViewModel), result, "return type not correct");
            Assert.AreEqual("Dave", result.FirstName);
            Assert.AreEqual("Mateer", result.LastName);
        }
    }
}
