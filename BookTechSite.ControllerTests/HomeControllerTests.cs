﻿using System.Web.Mvc;
using BookTechSite.Controllers;
using NUnit.Framework;

namespace BookTechSite.ControllerTests
{
    [TestFixture]
    public class HomeControllerTests
    {
        // Want the controller methods to be skinny, so not much to test
        // ideally just want to make sure they render!
        [Test]
        public void index_should_render_default_view()
        {
            HomeController hc = new HomeController();

            ViewResult result = (ViewResult)hc.Index();

            Assert.IsNotNull(result);
        }
    }
}
