﻿using System;
using BookTechSite.Models;
using BookTechSite.Services;
using NUnit.Framework;

// Describe the business problem I am trying to solve

// Search just filter down the same page layout as home

// If the admin presses the Author box to Authorise payment
// and this is the first Sale of the Book
// should send out congratulations email

// Default view of Author screen is only to show Authors who have Books.

// should be a screen to see what emails have been sent

// Check for new book sales button should look at shared directory
// and read files, then parse, to see if there are any new sales
// to import into the system

// check for new book sales should be runnable from a console application too
// for automated checking

// check for new book sales should run thorugh a Filter (as there are many hundreds of thousands of sales per day)
// and just want ones for the books we are 
// importer should be solid and be able to cope with failures
// should log import to a table?

// Reset database button should delete everything in the db
// and put in sample data
// want

namespace BookTechSite.UnitTests
{
    [TestFixture]
    public class BPAServiceTests
    {
        [Test]
        [Ignore("Not implemented")]
        public void given_first_book_sale_should_give_total_book_sales_of_1()
        {
            var author = new Author { AuthorID = 1 };
            var book = new Book { BookID = 2, Title = "test", AuthorID = author.AuthorID };
            var sale = new Sale { BookID = book.BookID, Price = 7.99D, SaleDate = new DateTime(2016, 02, 22, 09, 00, 00), AuthorSaleStatus = (int)AuthorSaleStatus.AuthorNotPaid};

            // need to inject in a fake SaleRepostory?
            var service = new BPAService();
            //int bookSales = service.GetTotalBookSalesByBookID(book.BookID);

            //Assert.AreEqual(1, bookSales);
        }

        [Test]
        [Ignore("Not implemented")]
        public void given_first_book_sale_should_be_set_to_author_not_paid()
        {
            
        }

        [Test]
        [Ignore("Not implemented")]
        public void given_first_book_sale_number_of_author_not_paid_books_should_be_1()
        {
            
        }

        //public void given_first_book_sale_payment_authorised_button_press_should_send_congratulations_email_to_author()


        //[Test]
        //public void given_first_book_sale_should_return_true_for_book_isFirstBookSale()
        //{
        //    var author = new Author();
        //    //author.Books = new List<Book> { new Book { //NumberOfSales = 0 } };

        //    // test to see if this Author should be send a congratulations email
        //    bool isFirstBookSale = Book.IsFirstBookSale();

        //    Assert.AreEqual(true, isFirstBookSale);

        //    //var emailler = new Emailler();
        //    //emailler.SendEmail(author.EmailAddress, from: "admin@bookpublishingcorp.com", message: "Well done on your first sale!");
        //}

        //[Test]
        //public void given_second_book_sale_should_return_false_for_book_isFirstBookSale()
        //{
        //    var author = new Author();
        //    //author.Books = new List<Book> { new Book { NumberOfSales = 1 } };

        //    // test to see if this Author should be send a congratulations email
        //    bool isFirstBookSale = Book.IsFirstBookSale();

        //    Assert.AreEqual(true, isFirstBookSale);

        //    //var emailler = new Emailler();
        //    //emailler.SendEmail(author.EmailAddress, from: "admin@bookpublishingcorp.com", message: "Well done on your first sale!");
        //}
    }

    
}
